﻿## Overview

This little project contains 2 independent parts: Minesweeper and Battleship.

1. Make the code review and describe, what's wrong with them. The aim is to find code smells.
2. Try to refactor and write a unit test that checks ships are unable to intersect.
